require 'active_record'
require 'action_controller'

class ActiveRecord::ConnectionAdapters::AbstractAdapter
  def log_info_with_backtrace(sql, name, runtime)
    log_info_without_backtrace(sql, name, runtime)
    
    return unless @logger and @logger.debug? and ActiveRecord::Base.log_sql_backtrace
    return if / Columns$/ =~ name

    @logger.debug(format_backtrace(clean_backtrace))
  end
  
  alias_method_chain :log_info, :backtrace
  
  def format_backtrace(backtrace)
    if ActiveRecord::Base.colorize_logging
      if @@row_even
        message_color = "35;2"
      else
        message_color = "36;2"
      end
      backtrace.collect { |t| "    \e[#{message_color}m#{t}\e[0m" }.join("\n")
    else
      backtrace.join("\n    ")
    end
  end
  
  VENDOR_RAILS_REGEXP = %r(([\\/:])vendor\1rails\1)
  PLUGIN_REGEXP = %r(([\\/:])vendor\1plugins\1sql_logging\1)
  
  def clean_backtrace(backtrace = caller)
    return backtrace unless defined?(RAILS_ROOT)

    root = File.expand_path(RAILS_ROOT)
    backtrace.select { |t| /#{Regexp.escape(root)}/ =~ t }.
      reject { |t| VENDOR_RAILS_REGEXP =~ t || PLUGIN_REGEXP =~ t }.
      collect { |t| t.gsub(root + '/', '') }
  end
end

module SqlBacktraceSilencer
  def silence_sql_backtrace
    log_backtrace = ActiveRecord::Base.log_sql_backtrace
    begin
      ActiveRecord::Base.log_sql_backtrace = false
      yield
    ensure
      ActiveRecord::Base.log_sql_backtrace = log_backtrace
    end
  end
end

class ActiveRecord::Base
  @@log_sql_backtrace = true
  cattr_accessor :log_sql_backtrace
  
  include SqlBacktraceSilencer
end

class ActionController::Base
  include SqlBacktraceSilencer
end
