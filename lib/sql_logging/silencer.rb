require 'active_record'
require 'action_controller'

class ActiveRecord::ConnectionAdapters::AbstractAdapter
  def log_info_with_silencing(sql, name, runtime)
    if ActiveRecord::Base.log_sql_query
      log_info_without_silencing(sql, name, runtime)
    end
  end
  
  alias_method_chain :log_info, :silencing
end

module SqlInfoSilencer
  def silence_sql
    logging = ActiveRecord::Base.log_sql_query
    begin
      ActiveRecord::Base.log_sql_query = false
      yield
    ensure
      ActiveRecord::Base.log_sql_query = logging
    end
  end
end

class ActiveRecord::Base
  @@log_sql_query = true
  cattr_accessor :log_sql_query
  
  include SqlInfoSilencer
end

class ActionController::Base
  include SqlInfoSilencer
end
