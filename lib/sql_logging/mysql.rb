require 'active_record/connection_adapters/mysql_adapter'

class ActiveRecord::ConnectionAdapters::MysqlAdapter
  def select_with_stats(sql, name = nil)
    rows = nil
    elapsed = Benchmark.measure do
      rows = select_without_stats(sql, name)
    end
    msec = (elapsed.real * 1000).to_i
    record_query_stats(sql, name, msec, rows)
    rows
  end
  
  alias_method_chain :select, :stats
  alias_method_chain :execute, :stats
end
